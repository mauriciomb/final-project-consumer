package com.consumer.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "produce_message")
public class ProduceMessage implements Serializable {

    private static final long serialVersionUID = -5596749208426419390L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(name="transaction_id")
    private String transactionId;

    @Column(name="message")
    private String message;

    @Column(name="messageType")
    private String messageType;

    @Column(name="consumed")
    private Boolean consumed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Boolean getConsumed() {
        return consumed;
    }

    public void setConsumed(Boolean consumed) {
        this.consumed = consumed;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProduceMessage that = (ProduceMessage) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        return messageType != null ? messageType.equals(that.messageType) : that.messageType == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (messageType != null ? messageType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProduceMessage{");
        sb.append("id=").append(id);
        sb.append(", transactionId='").append(transactionId).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append(", messageType='").append(messageType).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

