package com.consumer.service;

import com.consumer.db.HibernateUtil;
import com.consumer.entity.ProduceMessage;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class ConsumerServiceAsync {

    static Logger logger = Logger.getLogger(ConsumerServiceAsync.class);

    public void consumer() {
        String hql = "FROM ProduceMessage where consumed = false";
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session s = sf.openSession();
        while (true){
            logger.info("Looking for messages to consume...");
            Query query = s.createQuery(hql);
            List<ProduceMessage> results = query.list();
            for (ProduceMessage message : results){
                logger.info("Message consumed: " + message);
                //updating the status
                message.setConsumed(true);
                Transaction transaction = s.beginTransaction();
                s.save(message);
                transaction.commit();
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
