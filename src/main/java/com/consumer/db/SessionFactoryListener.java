package com.consumer.db;

import com.consumer.service.ConsumerServiceAsync;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class SessionFactoryListener implements ServletContextListener {

    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        final ConsumerServiceAsync consumerServiceAsync = new ConsumerServiceAsync();
        Runnable consumerAsync = new Runnable() {
            @Override
            public void run() {
                consumerServiceAsync.consumer();
            }
        };
        Thread consumerThread = new Thread(consumerAsync);
        consumerThread.start();
    }
}
